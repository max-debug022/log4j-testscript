import spark.Spark;

import java.io.File;
import java.nio.file.Files;

public class HTTP_Server
{
    public static void main(String[] args) {
        // Prints entry message
    	System.out.println("Payload Server started!");

        // Sets port listener
        Spark.port(8080);

        // Maps the route for HTTP requests
        Spark.get("/Payload.class", (req, res) -> {
        	System.err.println("Payload requested!");
            // Return the Payload as Bytes
            return Files.readAllBytes(new File("target/classes/Payload.class").toPath());
        });
    }
}
