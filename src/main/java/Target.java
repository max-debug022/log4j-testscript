import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import spark.Spark;


public class Target {
    static Logger log = LogManager.getLogger(Target.class.getName());

    public static void main(String[] args) {
        // Print Console Message
    	System.out.println("Defender Server Started!");

        // Configure Port-Listener
        Spark.port(8000);

        // Map the route for http requests on "localhost/home"
        Spark.get("/home", (req, res) -> {
            // Get Parameter "param"
            String name = req.queryParams("param");
            // Log the request
            log.info("The last request was: " + name);

            // Returns your request
            return name;
        });
    }
}
